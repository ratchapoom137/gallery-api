package header

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

// GetToken -
func GetToken(c *gin.Context) string {
	header := c.GetHeader("Authorization")
	header = strings.TrimSpace(header)
	fmt.Print("header: ", header)
	min := len("Bearer ")
	if len(header) <= min {
		return ""
	}

	return header[min:]
}
