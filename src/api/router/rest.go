package router

import (
	"api/handlers"
	"api/middleware"
	"api/migration"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func RestRouter(config cors.Config) *gin.Engine {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	db.LogMode(true) // dev only!

	r := gin.Default()
	r.Use(cors.New(config))

	//Migration Database
	r.GET("/migrate", func(c *gin.Context) {
		migration.Execute()

		c.JSON(200, gin.H{
			"message": "Migration Gallery Database Successfull",
		})
	})

	// tg := services.NewGalleryGorm(db)
	// th := handlers.NewGalleryHandler(tg)

	r.Static("/upload", "./upload")

	r.POST("/register", handlers.CreateUserHandler)

	r.POST("/login", handlers.LoginHandler)

	auth := r.Group("/auth")

	auth.Use(middleware.RequireUser)
	{
		//Gallery
		auth.POST("/gallery", handlers.CreateGalleryHandler)
		auth.GET("/gallery/:id", handlers.GetGalleryByIDHandler)
		auth.GET("/galleries", handlers.GetGalleryListHandler)
		auth.DELETE("/gallery/:id", handlers.DeleteGalleryByIDHandler)
		auth.PATCH("/gallery/:id", handlers.UpdateGalleryHandler)
		auth.PATCH("/gallery/:id/public", handlers.UpdateGalleryPublicHandler)

		//Images
		auth.POST("/gallery-image/:gallery_id", handlers.CreateImageHandler)
		auth.GET("/gallery-image/:id", handlers.GetImageByIDHandler)
		auth.GET("/gallery-images", handlers.GetImageListHandler)
		auth.PATCH("/gallery-image/:id", handlers.UpdateImageHandler)
		auth.DELETE("/gallery-image/:id", handlers.DeleteImageHandler)

		//User
		auth.PATCH("/user", handlers.UpdateUserHandler)

		//Like
		auth.POST("/image/:image_id/like", handlers.CreateLikeHandler)
		auth.DELETE("/image-like/:id", handlers.DeleteLikeHandler)

		// //Comment
		// auth.POST("/image/:image_id/comment", handlers.CreateCommentHandler)
		// auth.PATCH("/image-comment/:id", handlers.CreateCommentHandler)
		// auth.DELETE("/image-comment/:id", handlers.DeleteCommentHandler)
	}

	// r.PATCH("/tasks/:id", th.UpdateTaskFn)

	// r.POST("/gallery", th.CreateGalleryHandler)

	return r
}
