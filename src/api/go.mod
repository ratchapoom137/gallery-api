module api

go 1.13

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.2.0
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.5.2 // indirect
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	gopkg.in/gormigrate.v1 v1.6.0
)
