package services

import (
	"api/models"
	"encoding/json"
	"errors"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// CreateUser -
func CreateUser(userInput *models.User) (err error) {

	hash, err := bcrypt.GenerateFromPassword([]byte(userInput.Password), bcrypt.MinCost)

	userInput.Password = string(hash)

	token, err := RandomToken()
	userInput.Token = token

	tokenAndHash := TokenAndHash(token)

	userSave := new(models.User)

	userSave.Email = userInput.Email
	userSave.Password = string(hash)
	userSave.Token = tokenAndHash

	if err = userSave.Create(); err != nil {
		return err
	}

	userInput.ID = userSave.ID
	userInput.FirstName = userSave.FirstName
	userInput.LastName = userSave.LastName
	userInput.URL = userSave.URL

	return
}

func GetUserByID(userID int) (result models.User, err error) {
	filter := userSearchFilter{
		id: &userID,
	}

	output, searchErr := searchUserWithPreload(withFilter(filter))
	if output.total == 1 {
		userResult := output.result.([]models.User)[0]

		var userResultParsed models.User
		jsonUserResult, _ := json.Marshal(userResult)
		json.Unmarshal(jsonUserResult, &userResultParsed)

		// userResultParsed.GetOutput("users")
		result = userResultParsed
		return
	}

	err = searchErr
	return
}

// GetUserList -
func GetUserList(filter map[string]interface{}) (result []models.User, err error, total int) {

	var userFilter userSearchFilter

	if email, ok := filter["email"].(string); ok && email != "" {
		userFilter.email = &email
	}

	if token, ok := filter["token"].(string); ok && token != "" {
		tokenAndHash := TokenAndHash(token)
		userFilter.token = &tokenAndHash
	}
	limit, limitOk := filter["limit"].(int)
	if !limitOk {
		limit = 10
	}
	offset, offsetOk := filter["offset"].(int)
	if !offsetOk {
		offset = 0
	}

	output, searchErr := searchUserWithPreload(withOffset(offset), withLimit(limit), withFilter(userFilter))

	if output.total >= 1 {
		result = output.result.([]models.User)
		total = output.total

		return
	}

	err = searchErr
	return
}

// Login -
func Login(userInput *models.User) error {

	limit := 1
	offset := 0

	var userFilter userSearchFilter
	if userInput.Email != "" {
		userFilter.email = &userInput.Email
	}

	output, searchErr := searchUserWithPreload(withOffset(offset), withLimit(limit), withFilter(userFilter))
	if searchErr != nil {
		return searchErr
	}
	if output.total <= 0 {
		return errors.New("Invalid email or password. Please try again.")
	}
	result := output.result.([]models.User)[0]

	isSuccessd := comparePasswords(result.Password, userInput.Password)

	if !isSuccessd {
		return errors.New("Invalid email or password. Please try again.")
	}

	token, err := RandomToken()
	userInput.Token = token
	if err != nil {
		return errors.New("can not random token.")
	}

	tokenAndHash := TokenAndHash(token)

	var userUpdate models.User
	userUpdate.ID = result.ID
	userUpdate.Token = tokenAndHash

	if err := userUpdate.Update(); err != nil {
		return err
	}

	userInput.ID = result.ID
	userInput.FirstName = result.FirstName
	userInput.LastName = result.LastName
	userInput.URL = result.URL

	return nil
}

const UploadPathUser = "upload/user"

func UpdateUser(forms *multipart.Form, userID int) (result models.User, err error) {

	if forms.File["photo"] != nil {
		idStr := strconv.FormatUint(uint64(userID), 10)
		dir := filepath.Join(UploadPathUser, idStr)
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			log.Printf("create user dir error: %v\n", err)
			return result, err
		}

		db, err := gorm.Open(
			"mysql",
			"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
		)

		tx := db.Begin()
		defer func() {
			if r := recover(); r != nil {
				log.Println("rollback due to error while uploading photo")
				tx.Rollback()
			}
		}()

		if err := tx.Error; err != nil {
			log.Printf("transaction error: %v\n", err)
			return result, err
		}

		file := forms.File["photo"]
		generated, err := RandomToken()
		if err != nil {
			log.Printf("error generating filename: %v\n", err)
			tx.Rollback()
			return result, err
		}
		ext := filepath.Ext(file[0].Filename)
		user := models.User{
			ID:  userID,
			URL: generated[:len(generated)-1] + ext,
		}

		userInfo, err := GetUserByID(userID)
		if err != nil {
			return result, err
		}

		if userInfo.URL != "" {
			os.Remove(userInfo.FilePath())
		}

		if err := saveFile(file[0], user.FilePath()); err != nil {
			tx.Rollback()
			return result, err
		}

		if err := user.Update(); err != nil {
			tx.Rollback()
			return result, err
		}
		result.URL = user.URL
		return result, err
	}

	var userUpdate models.User
	userUpdate.ID = userID

	if len(forms.Value["first_name"]) >= 1 && forms.Value["first_name"][0] != "" {
		userUpdate.FirstName = forms.Value["first_name"][0]
	}
	if len(forms.Value["last_name"]) >= 1 && forms.Value["last_name"][0] != "" {
		userUpdate.LastName = forms.Value["last_name"][0]
	}
	if len(forms.Value["bio"]) >= 1 && forms.Value["bio"][0] != "" {
		userUpdate.Bio = forms.Value["bio"][0]
	}

	if err := userUpdate.Update(); err != nil {
		return result, err
	}
	result = userUpdate

	return result, err
}
