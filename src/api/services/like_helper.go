package services

import (
	"api/models"
	"log"

	"github.com/jinzhu/gorm"
)

type likeSearchFilter struct {
	image_id *int
	user_id  *int
}

func (like likeSearchFilter) Bind(db *gorm.DB) *gorm.DB {

	if like.image_id != nil {
		db = db.Where("image_id = ?", like.image_id)
	}

	if like.user_id != nil {
		db = db.Where("user_id = ?", like.user_id)
	}

	return db
}

func searchLikeWithPreload(options ...searchOption) (output searchResult, err error) {
	var likes []models.Like
	var count int

	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)
	db = db.Model(&models.Like{})

	for _, option := range options {
		db = option(db)
	}

	if outputErr := db.Order("id desc").Find(&likes).Error; outputErr != nil {
		log.Fatal(outputErr.Error())

		err = outputErr
		return
	}

	db.Limit(-1).Offset(0).Count(&count)

	output.result = likes
	output.total = count
	return
}
