package services

import (
	"api/models"
	"errors"
	"log"

	"github.com/jinzhu/gorm"
)

type imageSearchFilter struct {
	id         *int
	gallery_id *int
}

func (imgf imageSearchFilter) Bind(db *gorm.DB) *gorm.DB {

	if imgf.id != nil {
		db = db.Where("id = (?)", imgf.id)
	}

	if imgf.gallery_id != nil {
		db = db.Where("gallery_id = ?", *imgf.gallery_id)
	}

	return db
}

func searchImageWithPreload(options ...searchOption) (output searchResult, err error) {
	var images []models.Image
	var count int

	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)
	db = db.Model(&models.Image{})

	for _, option := range options {
		db = option(db)
	}

	if outputErr := db.Order("id desc").Find(&images).Error; outputErr != nil {
		log.Fatal(outputErr.Error())

		err = outputErr
		return
	}

	db.Limit(-1).Offset(0).Count(&count)

	output.result = images
	output.total = count
	return
}

func searchTotalImage(options ...searchOption) (output searchResult, err error) {
	// var images []models.Image
	var count int

	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)
	db = db.Model(&models.Image{})

	for _, option := range options {
		db = option(db)
	}

	if outputErr := db.Order("id desc").Error; outputErr != nil {
		log.Fatal(outputErr.Error())

		err = outputErr
		return
	}

	db.Limit(-1).Offset(0).Count(&count)

	output.total = count
	return
}

func CompareTokenUserIDAndImageUserID(galleryID int, tokenUserID int) (err error) {
	galleryRes, err := GetGalleryByID(galleryID)
	if err != nil {
		return
	}
	if galleryRes.UserID != tokenUserID {
		err = errors.New("No permission for update this gallery")
		return
	}

	return
}
