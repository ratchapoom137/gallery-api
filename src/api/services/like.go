package services

import (
	"api/models"
	"errors"
)

func CreateLike(like *models.Like) (err error) {

	likeFilter := likeSearchFilter{
		image_id: &like.ImageID,
		user_id:  &like.UserID,
	}

	output, _ := searchLikeWithPreload(withFilter(likeFilter))
	if output.total >= 1 {
		err = errors.New("your liked")
		return err
	}

	if err = like.Create(); err != nil {
		return err
	}

	return err
}

func DeleteLike(like *models.Like) (err error) {

	if err = like.Delete(); err != nil {
		return err
	}

	return err
}
