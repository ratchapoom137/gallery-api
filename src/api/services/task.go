package services

// import (
// 	"api/models"

// 	"github.com/jinzhu/gorm"
// )

// type TaskService interface {
// 	ListTask() ([]models.TaskTable, error)
// 	GetTaskByID(id int) (*models.TaskTable, error)
// 	CreateTask(task *models.TaskTable) error
// 	UpdateTask(task *models.TaskTable) error
// 	DeleteTask(id int) error
// }

// // var _ TaskService = &TaskGorm{}

// type TaskGorm struct {
// 	db *gorm.DB
// }

// func NewTaskGorm(db *gorm.DB) TaskService {
// 	return &TaskGorm{db}
// }

// func (tg *TaskGorm) CreateTask(task *models.TaskTable) error {
// 	return tg.db.Create(task).Error
// }

// func (tg *TaskGorm) GetTaskByID(id int) (*models.TaskTable, error) {
// 	var tt models.TaskTable

// 	if err := tg.db.First(&tt, id).Error; err != nil {
// 		return nil, err
// 	}
// 	return &tt, nil
// }

// func (tg *TaskGorm) ListTask() ([]models.TaskTable, error) {
// 	taskTables := []models.TaskTable{}
// 	if err := tg.db.Find(&taskTables).Error; err != nil {
// 		return nil, err
// 	}
// 	return taskTables, nil
// }

// func (tg *TaskGorm) UpdateTask(task *models.TaskTable) error {
// 	var found models.TaskTable
// 	if err := tg.db.Where("id = ?", task.ID).First(&found).Error; err != nil {
// 		return err
// 	}
// 	return tg.db.Model(task).Update("done", task.Done).Error
// }

// func (tg *TaskGorm) DeleteTask(id int) error {
// 	var tt models.TaskTable
// 	if err := tg.db.Where("id = ?", id).First(&tt).Error; err != nil {
// 		return err
// 	}
// 	return tg.db.Delete(tt).Error
// }
