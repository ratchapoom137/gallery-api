package services

import (
	"api/models"
	"encoding/json"
	"fmt"
	"strconv"
)

// CreateGallery is func for create gallery
func CreateGallery(gallery *models.Gallery) (err error) {
	gallery.IsPublic = true
	if err = gallery.Create(); err != nil {
		return err
	}

	return err
}

// GetGalleryByID -
func GetGalleryByID(id int) (result models.Gallery, err error) {

	filter := gallerySearchFilter{
		id: &id,
	}

	output, searchErr := searchGalleryWithPreload(withFilter(filter), withPreload("Images"))
	if output.total == 1 {
		galleryResult := output.result.([]models.Gallery)[0]

		var galleryResultParsed models.Gallery
		jsonGalleryResult, _ := json.Marshal(galleryResult)
		json.Unmarshal(jsonGalleryResult, &galleryResultParsed)

		// galleryResultParsed.GetOutput("galleries")
		result = galleryResultParsed
		return
	}

	err = searchErr
	return

}

// GetGalleryList -
func GetGalleryList(filter map[string]interface{}) (result []models.Gallery, err error, total int) {

	var glFilter gallerySearchFilter
	limit, _ := strconv.Atoi(filter["limit"].(string))
	offset, _ := strconv.Atoi(filter["offset"].(string))
	fmt.Print("filter11111: ", filter)

	if name, ok := filter["name"].(string); ok && name != "" {
		glFilter.name = &name
	}
	if isPublic, ok := filter["is_public"].(string); ok && isPublic != "" {
		if isPublic == "true" {
			b := true
			glFilter.is_public = &b
		}
	}
	if userID, ok := filter["user_id"].(int); ok && userID >= 1 {
		glFilter.user_id = &userID
	}

	output, searchErr := searchGalleryWithPreload(withOffset(offset), withLimit(limit), withFilter(glFilter), withPreload("Images"))
	if output.total >= 1 {
		result = output.result.([]models.Gallery)
		total = output.total

		return
	}

	err = searchErr
	return
}

func DeleteGalleryByID(id int, userLogin *models.User) (galleryID int, err error, total int) {

	images := models.Image{
		GalleryID: id,
	}

	err = CompareTokenUserIDAndGalleryUserID(id, userLogin.ID)
	if err != nil {
		return
	}

	if err = images.Delete(); err != nil {
		return
	}

	gallery := models.Gallery{
		ID: id,
	}
	if err = gallery.Delete(); err != nil {
		return
	}

	return gallery.ID, err, total
}

func UpdateGallery(galleryInput models.Gallery, userLogin *models.User) (err error) {

	err = CompareTokenUserIDAndGalleryUserID(galleryInput.ID, userLogin.ID)
	if err != nil {
		return err
	}

	if err = galleryInput.Update(); err != nil {
		return err
	}

	return
}

func UpdateGalleryPublic(galleryInput models.Gallery, userLogin *models.User) (err error) {

	err = CompareTokenUserIDAndGalleryUserID(galleryInput.ID, userLogin.ID)
	if err != nil {
		return err
	}

	if err = galleryInput.UpdatePublic(); err != nil {
		return err
	}

	return
}
