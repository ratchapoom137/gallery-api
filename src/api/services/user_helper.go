package services

import (
	"api/models"
	"encoding/base64"
	"log"

	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

const key = "secret_code"

type userSearchFilter struct {
	id          *int
	email       *string
	searchEmail *string
	token       *string
}

func (usf userSearchFilter) Bind(db *gorm.DB) *gorm.DB {
	// var user models.User

	if usf.id != nil {
		db = db.Where("id = (?)", *usf.id)
	}

	if usf.email != nil {
		db = db.Where("email = ?", *usf.email)
	}

	if usf.email != nil {
		db = db.Where("email LIKE ?", "%"+*usf.email+"%")
	}

	if usf.token != nil {
		db = db.Where("token = ?", *usf.token)
	}

	return db
}

func searchUserWithPreload(options ...searchOption) (output searchResult, err error) {
	var user []models.User
	var count int

	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)
	db = db.Model(&[]models.User{})

	for _, option := range options {
		db = option(db)
	}

	if outputErr := db.Preload("Role").Order("id desc").Find(&user).Error; outputErr != nil {
		log.Fatal(outputErr.Error())
		err = outputErr
		return
	}

	db.Limit(-1).Offset(0).Count(&count)

	output.result = user
	output.total = count
	return
}

func TokenAndHash(token string) string {
	mac := hmac.New(sha256.New, []byte(key))
	mac.Reset()
	mac.Write([]byte(token))
	newToken := mac.Sum(nil)
	return base64.URLEncoding.EncodeToString(newToken)
}

func RandomToken() (string, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), err
}

// Helper
func comparePasswords(hashedPwd string, plainPwdStr string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwdStr)); err != nil {
		log.Println(err)
		return false
	}

	return true
}

func hashAndSalt(pwd string) string {
	// Return the users input as a byte slice which will save us
	// from having to do this conversion later on
	pwdByte := []byte(pwd)

	// Use GenerateFromPassword to hash & salt pwd.
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(pwdByte, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	// GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash)
}
