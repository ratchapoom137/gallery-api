package services

import (
	"api/models"
	"encoding/json"
	"fmt"
	"io"

	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

const UploadPath = "upload"

func CreateImages(forms *multipart.Form, galleryID int, userLogin *models.User) (err error) {

	err = CompareTokenUserIDAndImageUserID(galleryID, userLogin.ID)
	if err != nil {
		return
	}

	idStr := strconv.FormatUint(uint64(galleryID), 10)
	dir := filepath.Join(UploadPath, idStr)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		log.Printf("create gallery dir error: %v\n", err)
		return err
	}

	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			log.Println("rollback due to error while uploading photo")
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		log.Printf("transaction error: %v\n", err)
		return err
	}

	images := []models.Image{}
	for index, file := range forms.File["photos"] {
		generated, err := RandomToken()
		if err != nil {
			log.Printf("error generating filename: %v\n", err)
			tx.Rollback()
			return err
		}
		ext := filepath.Ext(file.Filename)
		image := models.Image{
			GalleryID:   galleryID,
			Description: forms.Value["description"][index],
			URL:         generated[:len(generated)-1] + ext,
			CreatedBy:   userLogin.ID,
			UpdatedBy:   userLogin.ID,
		}
		if err := tx.Create(&image).Error; err != nil {
			log.Printf("create image error: %v\n", err)
			tx.Rollback()
			return err
		}
		images = append(images, image)

		if err := saveFile(file, image.FilePath()); err != nil {
			tx.Rollback()
			return err
		}
	}

	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return err
	}

	return err
}

func saveFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		log.Printf("saveFile - open file error: %v\n", err)
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		log.Printf("saveFile - create destination file error: %v\n", err)
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	if err != nil {
		log.Printf("saveFile - copy file error: %v\n", err)
	}
	return err
}

func GetImageByID(imgID int) (result models.Image, err error) {
	filter := imageSearchFilter{
		id: &imgID,
	}

	output, searchErr := searchImageWithPreload(withFilter(filter), withPreload("Comments"), withPreload("Likes.User"))
	if output.total == 1 {
		imageResult := output.result.([]models.Image)[0]

		var imageResultParsed models.Image
		jsonImageResult, _ := json.Marshal(imageResult)
		json.Unmarshal(jsonImageResult, &imageResultParsed)

		// galleryResultParsed.GetOutput("galleries")
		result = imageResultParsed
		return
	}

	err = searchErr
	return
}

func GetImageList(filter map[string]interface{}) (result []models.Image, err error, total int) {
	var imgFilter imageSearchFilter
	limit, _ := strconv.Atoi(filter["limit"].(string))
	offset, _ := strconv.Atoi(filter["offset"].(string))

	if galleryID, ok := filter["gallery_id"].(int); ok && galleryID >= 1 {
		imgFilter.gallery_id = &galleryID
	}

	output, searchErr := searchImageWithPreload(withOffset(offset), withLimit(limit), withFilter(imgFilter))
	if output.total >= 1 {
		fmt.Println("output55666: ", output)
		result = output.result.([]models.Image)
		total = output.total

		return
	}

	err = searchErr
	return
}

func DeleteImage(id int, userLogin *models.User) (result int, err error) {

	image := models.Image{
		ID: id,
	}

	imgResult, err := GetImageByID(id)
	if err != nil {
		return result, err
	}
	err = CompareTokenUserIDAndImageUserID(imgResult.GalleryID, userLogin.ID)
	if err != nil {
		return
	}

	image.GalleryID = imgResult.GalleryID
	image.URL = imgResult.URL
	os.Remove(image.FilePath())

	if err = image.Delete(); err != nil {
		return
	}

	return image.ID, err
}
