package services

import (
	"api/models"
	"errors"
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
)

type gallerySearchFilter struct {
	id        *int
	user_id   *int
	name      *string
	is_public *bool
}

func (gsf gallerySearchFilter) Bind(db *gorm.DB) *gorm.DB {
	var gallery models.Gallery

	if gsf.id != nil {
		db = db.Where("id = (?)", gsf.id).First(&gallery)
	}

	if gsf.name != nil {
		db = db.Where("name LIKE ?", "%"+*gsf.name+"%")
	}

	if gsf.is_public != nil {

		db = db.Where("is_public = ?", *gsf.is_public)
	}

	if gsf.user_id != nil {
		fmt.Println("userrr: ", *gsf.user_id)
		db = db.Where("user_id = (?)", *gsf.user_id)
	}

	return db
}

func searchGalleryWithPreload(options ...searchOption) (output searchResult, err error) {
	var gallery []models.Gallery
	var count int

	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)
	db = db.Model(&models.Gallery{})

	for _, option := range options {
		db = option(db)
	}

	if outputErr := db.Order("id desc").Find(&gallery).Error; outputErr != nil {
		log.Fatal(outputErr.Error())

		err = outputErr
		return
	}

	db.Limit(-1).Offset(0).Count(&count)

	// dbImg := db.Model(&models.Image{})
	// for _, item := range gallery {
	// 	var total int
	// 	dbImg.Where("gallery_id = ?", item.ID).Count(&total)
	// 	imageTotal = append(imageTotal, total)
	// }

	output.result = gallery
	output.total = count
	return
}

func CompareTokenUserIDAndGalleryUserID(galleryID int, tokenUserID int) (err error) {
	//Check user_id in token and user_id in gallery
	result, err := GetGalleryByID(galleryID)
	if err != nil {
		return
	}
	if result.UserID != tokenUserID {
		err = errors.New("No permission for update this gallery")
		return
	}

	return
}
