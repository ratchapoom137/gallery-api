package main

import (
	"api/router"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Cross Origin Resource Sharing

// 1. เพิ่ม axios.post ใน api.js
// 2. call function in React Component ใน App.js
// 3. intercept body ด้วย gin context c.BindJSON()
// 4. logging

func main() {

	// if err := db.AutoMigrate(
	// 	&models.TaskTable{},
	// ).Error; err != nil {
	// 	log.Fatal(err)
	// }

	//Gin
	// r := gin.Default()
	// config := cors.DefaultConfig()
	// config.AllowOrigins = []string{"http://localhost:3000"}
	// r.Use(cors.New(config))

	ginApp := router.New()

	ginApp.Run()
}
