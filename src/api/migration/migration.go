package migration

import (
	"api/models"
	"log"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

//Execute - execute migration database of gallery api
func Execute() {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)
	if err != nil {
		log.Fatal("can not connect database", err)
		return
	}
	defer db.Close()
	db.LogMode(true) // dev only!

	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID:       "202005271800",
			Migrate:  Migrate202005271800,
			Rollback: Rollback202005271800,
		},
		{
			ID:       "202005281800",
			Migrate:  Seed202005281800,
			Rollback: Rollback202005281800,
		},
	})

	if err := m.Migrate(); err != nil {
		log.Fatal("Could not migrate: ", err)
		return
	}

	log.Printf("Migration done.")
}

//Migrate202005271800 - automigrate database
func Migrate202005271800(tx *gorm.DB) error {
	tx = tx.Begin()

	// tx.AutoMigrate(&models.TaskTable{})
	tx.AutoMigrate(
		&models.Role{},
		// &models.TaskTable{},
		&models.User{},
		&models.Comment{},
		&models.Image{},
		&models.Gallery{},
		&models.Like{},
	)

	return tx.Commit().Error
}

//Rollback202005271800 - drop table when migration database
func Rollback202005271800(tx *gorm.DB) error {
	tx = tx.Begin()

	tx.DropTable(
		&models.Role{},
		// &models.TaskTable{},
		&models.User{},
		&models.Comment{},
		&models.Image{},
		&models.Gallery{},
		&models.Like{},
	)

	return tx.Commit().Error
}

func Seed202005281800(tx *gorm.DB) error {
	tx = tx.Begin()

	// images1 := []models.Image{
	// 	models.Image{
	// 		Description: "aaaaaaaa",
	// 		URL:         "www.poom1.com",
	// 	},
	// 	models.Image{
	// 		Description: "bbbbbbbb",
	// 		URL:         "www.poom2.com",
	// 	},
	// }

	//Gallery1
	grallery1 := models.Gallery{
		Name:   "ggg jaaa",
		UserID: 1,
		// Images: images1,
	}

	role1 := models.Role{
		Name: "admin",
	}

	role2 := models.Role{
		Name: "user",
	}

	user1 := models.User{
		Email:     "admin",
		Password:  "$2a$04$4CGWqgtvCV4ouMoHb.sWkutfQ7Gvju2JHysV2L6UOMFNFgtz3fgd.",
		FirstName: "super",
		LastName:  "admin",
		RoleID:    1,
	}

	tx.Create(&grallery1)
	tx.Create(&role1)
	tx.Create(&role2)
	tx.Create(&user1)
	return tx.Commit().Error
}

func Rollback202005281800(tx *gorm.DB) error {
	return tx.Rollback().Error
}
