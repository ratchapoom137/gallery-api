package models

import "time"

//Comment -
type Comment struct {
	ID          int        `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	ImageID     int        `json:"image_id" gorm:"column:image_id;AUTO_INCREMENT"`
	Description string     `json:"description" gorm:"column:description"`
	UserID      int        `json:"user_id" gorm:"column:user_id"`
	User        *User      `json:"user,omitempty" gorm:"foreignkey:user_id;association_foreignkey:id;""`
	CreatedBy   int        `json:"created_by" sql:"not null"`
	UpdatedBy   int        `json:"updated_by" sql:"not null"`
	CreatedAt   time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt   time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt   *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

//CommentName -
func (Comment) CommentName() string {
	return "comments"
}
