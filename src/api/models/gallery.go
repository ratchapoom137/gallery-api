package models

import (
	"errors"
	"log"
	"time"

	validator "github.com/go-playground/validator/v10"
	"github.com/jinzhu/gorm"
)

type Gallery struct {
	ID        int        `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	UserID    int        `json:"user_id" gorm:"user_id"`
	Name      string     `json:"name" gorm:"column:name"`
	Images    []Image    `json:"images" gorm:"column:images;one2many:images;foreignkey:gallery_id;association_foreignkey:id;"`
	IsPublic  bool       `json:"is_public" gorm:"column:is_public"`
	CreatedBy int        `json:"created_by" sql:"not null"`
	UpdatedBy int        `json:"updated_by" sql:"not null"`
	CreatedAt time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

func (Gallery) GalleryName() string {
	return "galleries"
}

//Create Validation -
func (gl *Gallery) Create() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	validate := validator.New()
	err = validate.Struct(gl)
	if err != nil {
		log.Fatal(err.Error())
		return errors.New("invalid json")
	}

	if err = db.Create(&gl).Error; err != nil {
		return
	}

	return
}

func (gl *Gallery) Update() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	validate := validator.New()
	err = validate.Struct(gl)
	if err != nil {
		log.Fatal(err.Error())
		return errors.New("invalid json")
	}

	if err = db.Model(Gallery{ID: gl.ID}).Update("name", gl.Name).Error; err != nil {
		return
	}

	return
}

func (gl *Gallery) UpdatePublic() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	validate := validator.New()
	err = validate.Struct(gl)
	if err != nil {
		log.Fatal(err.Error())
		return errors.New("invalid json")
	}

	if err = db.Model(Gallery{ID: gl.ID}).Update("is_public", gl.IsPublic).Error; err != nil {
		return
	}

	return
}

func (gl *Gallery) Delete() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	if err = db.Where("id = ?", gl.ID).Delete(&Gallery{}).Error; err != nil {
		return
	}

	return
}

func (glr Gallery) GetOutput(gallery string) (glrOject interface{}) {
	glrOject = glr
	return
}
