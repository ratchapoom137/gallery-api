package models

import (
	"errors"
	"log"
	"path/filepath"
	"strconv"
	"time"

	validator "github.com/go-playground/validator/v10"
	"github.com/jinzhu/gorm"
)

const UploadPathUser = "upload/user"

//User -
type User struct {
	ID        int    `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	Email     string `json:"email" gorm:"column:email"`
	Password  string `json:"password" gorm:"column:password"`
	FirstName string `json:"first_name" gorm:"column:first_name"`
	LastName  string `json:"last_name" gorm:"column:last_name"`
	Bio       string `json:"bio" gorm:"column:bio"`
	URL       string `json:"url" gorm:"column:url"`
	Token     string `json:"token" gorm:"column:token";unique_index`
	RoleID    int    `json:"role_id" gorm:"column:role_id"`
	Role      *Role  `json:"role,omitempty" gorm:"foreignkey:role_id;association_foreignkey:id;"`
	// Followers []UserID  `json:"followers" gorm:"column:followers;one2many:users_id;association_foreignkey:user_id;"`
	// Following []UserID  `json:"following" gorm:"column:following;one2many:users_id;association_foreignkey:user_id;"`
	CreatedBy int        `json:"created_by" sql:"not null"`
	UpdatedBy int        `json:"updated_by" sql:"not null"`
	CreatedAt time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

func (user *User) FilePath() string {
	idStr := strconv.FormatUint(uint64(user.ID), 10)
	return filepath.Join(UploadPathUser, idStr, user.URL)
}

//Create Validation -
func (user *User) Create() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	validate := validator.New()
	err = validate.Struct(user)
	if err != nil {
		log.Fatal(err.Error())
		return errors.New("invalid json")
	}

	user.RoleID = 2
	user.FirstName = "คำปู๋จู้"
	user.LastName = "อิอิ"

	if err = db.Create(&user).Error; err != nil {
		return
	}

	return
}

// // Update
// func (user *User) Edit() (err error) {
// 	app := core.GetApplication()
// 	db := app.DB
// 	validate := validator.New()
// 	err = validate.Struct(user)
// 	if err != nil {
// 		app.Logger.Error(err.Error())
// 		err = errors.New("invalid json")
// 		return
// 	}
// 	if result := db.Model(User{ID: user.ID}).Save(&user); result.Error != nil {
// 		err = result.Error
// 		return
// 	}
// 	return
// }

func (user *User) Update() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	validate := validator.New()
	err = validate.Struct(user)
	if err != nil {
		log.Fatal(err.Error())
		return errors.New("invalid json")
	}

	if user.FirstName != "" {
		if err = db.Model(User{ID: user.ID}).Update("first_name", user.FirstName).Error; err != nil {
			return
		}
	}
	if user.LastName != "" {
		if err = db.Model(User{ID: user.ID}).Update("last_name", user.LastName).Error; err != nil {
			return
		}
	}
	if user.Token != "" {
		if err = db.Model(User{ID: user.ID}).Update("token", user.Token).Error; err != nil {
			return
		}
	}
	if user.URL != "" {
		if err = db.Model(User{ID: user.ID}).Update("url", user.URL).Error; err != nil {
			return
		}
	}
	if user.Bio != "" {
		if err = db.Model(User{ID: user.ID}).Update("bio", user.Bio).Error; err != nil {
			return
		}
	}

	return
}

// Delete is delete user
func (user *User) Delete() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	if err = db.Where("id = ?", user.ID).Delete(&User{}).Error; err != nil {
		return
	}

	return
}

//UserName -
func (User) UserName() string {
	return "users"
}

func ifStr(condition bool, ifTrue string, ifFalse string) string {
	if condition {
		return ifTrue
	}
	return ifFalse
}
