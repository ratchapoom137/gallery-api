package models

import "time"

//Role -
type Role struct {
	ID        int        `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	Name      string     `json:"name" gorm:"column:name"`
	CreatedBy int        `json:"created_by" sql:"not null"`
	UpdatedBy int        `json:"updated_by" sql:"not null"`
	CreatedAt time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

//RoleName -
func (Role) RoleName() string {
	return "roles"
}
