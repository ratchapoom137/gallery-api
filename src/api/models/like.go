package models

import (
	"errors"
	"log"
	"time"

	validator "github.com/go-playground/validator/v10"
	"github.com/jinzhu/gorm"
)

type Like struct {
	ID        int       `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	ImageID   int       `json:"image_id" gorm:"column:image_id;AUTO_INCREMENT"`
	UserID    int       `json:"user_id" gorm:"column:user_id"`
	User      *User     `json:"user" gorm:"foreignkey:user_id;association_foreignkey:id;""`
	CreatedBy int       `json:"created_by" sql:"not null"`
	UpdatedBy int       `json:"updated_by" sql:"not null"`
	CreatedAt time.Time `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt time.Time `json:"updated_at" sql:"type:datetime(6)"`
	// DeletedAt *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

func (Like) LikeName() string {
	return "likes"
}

//Create Validation -
func (like *Like) Create() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	validate := validator.New()
	err = validate.Struct(like)
	if err != nil {
		log.Fatal(err.Error())
		return errors.New("invalid json")
	}

	if err = db.Create(&like).Error; err != nil {
		return
	}

	return
}

func (like *Like) Delete() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	if err = db.Delete(&like).Error; err != nil {
		return
	}

	return
}
