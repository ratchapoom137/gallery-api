package models

import (
	"path/filepath"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
)

const UploadPath = "upload"

type Image struct {
	ID          int        `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	GalleryID   int        `json:"gallery_id" gorm:"column:gallery_id;AUTO_INCREMENT"`
	Description string     `json:"description" gorm:"column:description"`
	URL         string     `json:"url" gorm:"column:url"`
	Comments    []Comment  `json:"comments" gorm:"column:comments;one2many:comments;foreignkey:image_id;association_foreignkey:id;"`
	Likes       []Like     `json:"likes" gorm:"column:likes;one2many:likes;foreignkey:image_id;association_foreignkey:id;"`
	CreatedBy   int        `json:"created_by" sql:"not null"`
	UpdatedBy   int        `json:"updated_by" sql:"not null"`
	CreatedAt   time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt   time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt   *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

func (Image) ImageName() string {
	return "images"
}

func (img *Image) FilePath() string {
	idStr := strconv.FormatUint(uint64(img.GalleryID), 10)
	return filepath.Join(UploadPath, idStr, img.URL)
}

func (img *Image) Delete() (err error) {
	db, err := gorm.Open(
		"mysql",
		"admin:password@tcp(gallery-mysql)/gallery?charset=utf8mb4&parseTime=true",
	)

	if img.ID >= 1 {
		if err = db.Where("id = ?", img.ID).Delete(&Image{}).Error; err != nil {
			return
		}
	}

	if img.GalleryID >= 1 {
		if err = db.Where("gallery_id = ?", img.GalleryID).Delete(&Image{}).Error; err != nil {
			return
		}
	}

	return
}
