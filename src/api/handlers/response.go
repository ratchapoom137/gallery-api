package handlers

// APIResult -
type APIResult struct {
	Data   interface{}     `json:"data"`
	Total  int             `json:"total"`
	Status APIResultStatus `json:"status"`
}

// APIResultStatus -
type APIResultStatus struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}
