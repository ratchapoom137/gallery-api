package handlers

import (
	"api/context"
	"api/services"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ImageOutput struct {
	ID          int              `json:"id"`
	GalleryID   int              `json:"gallery_id"`
	Description string           `json:"description"`
	URL         string           `json:"url"`
	Comments    []CommentsOutput `json:"comments"`
	Likes       []LikesOutput    `json:"likes"`
}

type CommentsOutput struct {
	ID          int                `json:"id"`
	ImageID     int                `json:"image_id"`
	Description string             `json:"description"`
	User        *UserOutputInImage `json:"user,omitempty"`
}

type LikesOutput struct {
	ID      int                `json:"id"`
	ImageID int                `json:"image_id"`
	User    *UserOutputInImage `json:"user"`
}

type UserOutputInImage struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	URL       string `json:"url"`
}

func CreateImageHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)
	galleryID, err := strconv.Atoi(c.Param("gallery_id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	gallery, err := services.GetGalleryByID(galleryID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	err = services.CreateImages(form, gallery.ID, userLogin)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}

	c.JSON(201, APIResult{
		Status: APIResultStatus{
			Code:    "CREATE_IMAGES_SUCCESS",
			Message: "Create Images success",
		},
	})

	return
}

func GetImageByIDHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	imgID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	image, err := services.GetImageByID(imgID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	galleryRes, err := services.GetGalleryByID(image.GalleryID)
	if !galleryRes.IsPublic && galleryRes.UserID != userLogin.ID {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "No permission for get this gallery",
		})
		return
	}

	// err = CompareTokenUserIDAndImageUserID(image.GalleryID, userLogin.ID)
	// if err != nil {
	// 	return
	// }

	image.URL = image.FilePath()

	for _, item := range image.Likes {
		if item.User.URL != "" {
			item.User.URL = item.User.FilePath()
		}
	}

	var imageOutput ImageOutput
	jsonData, _ := json.Marshal(&image)
	json.Unmarshal(jsonData, &imageOutput)

	c.JSON(200, APIResult{
		Data:  imageOutput,
		Total: 1,
		Status: APIResultStatus{
			Code:    "GET_IMAGE_SUCCESS",
			Message: "Get image success",
		},
	})

	return
}

func GetImageListHandler(c *gin.Context) {
	// userLogin := context.GetUserLogin(c)

	galleryID, galleryIDOK := c.GetQuery("gallery_id")
	limit, limitOK := c.GetQuery("limit")
	offset, offsetOK := c.GetQuery("offset")

	filter := map[string]interface{}{}

	if galleryIDOK {
		filter["gallery_id"] = galleryID
	}
	if !limitOK && !offsetOK {
		limit = "-1"
		offset = "-1"
	}
	filter["limit"] = limit
	filter["offset"] = offset

	imageList, err, total := services.GetImageList(filter)

	for _, item := range imageList {
		item.URL = item.FilePath()
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, APIResult{
		Data:  imageList,
		Total: total,
		Status: APIResultStatus{
			Code:    "IMAGE_SEARCH_SUCCESS",
			Message: "Image search success",
		},
	})

	return
}

func UpdateImageHandler(c *gin.Context) {
	return
}

func DeleteImageHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	imageID, err := services.DeleteImage(id, userLogin)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, APIResult{
		Data: imageID,
		Status: APIResultStatus{
			Code:    "IMAGE_DELETE_SUCCESS",
			Message: "Image delete success",
		},
	})

	return
}
