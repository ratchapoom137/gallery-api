package handlers

import (
	"api/context"
	"api/models"
	"api/services"
	"encoding/json"
	"net/http"

	// "strconv"

	"github.com/gin-gonic/gin"
)

type UserOutput struct {
	ID        int    `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	FirstName string `json:"first_name" gorm:"column:first_name"`
	LastName  string `json:"last_name" gorm:"column:last_name"`
	Bio       string `json:"bio" gorm:"column:bio"`
	URL       string `json:"url" gorm:"column:url"`
}

// UserInputRegister -
type UserOutputLogin struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	URL       string `json:"url"`
	Token     string `json:"token"`
	Bio       string `json:"bio"`
}

// CreateUserHandler -
func CreateUserHandler(c *gin.Context) {
	var userInput *models.User
	if err := c.BindJSON(&userInput); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	if err := services.CreateUser(userInput); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}

	var userOutput UserOutputLogin
	jsonData, _ := json.Marshal(&userInput)
	json.Unmarshal(jsonData, &userOutput)

	c.JSON(201, APIResult{
		Data: userOutput,
		Status: APIResultStatus{
			Code:    "SIGNUP_SUCCESS",
			Message: "Sign up success",
		},
	})

	return
}

// LoginHandler -
func LoginHandler(c *gin.Context) {
	var userInput models.User
	err := c.BindJSON(&userInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	err = services.Login(&userInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	var userOutput UserOutputLogin
	jsonData, _ := json.Marshal(&userInput)
	json.Unmarshal(jsonData, &userOutput)

	if userInput.URL != "" {
		userOutput.URL = userInput.FilePath()
	}

	c.JSON(200, APIResult{
		Data: userOutput,
		Status: APIResultStatus{
			Code:    "LOGIN_SUCCESS",
			Message: "Login success",
		},
	})

	return
}

func UpdateUserHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	result, err := services.UpdateUser(form, userLogin.ID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}

	userLogin.URL = result.URL

	if result.URL != "" {
		result.URL = userLogin.FilePath()
	}

	var userOutput UserOutput
	jsonData, _ := json.Marshal(&result)
	json.Unmarshal(jsonData, &userOutput)

	c.JSON(200, APIResult{
		Data: userOutput,
		Status: APIResultStatus{
			Code:    "UPDATE_USER_SUCCESS",
			Message: "Update user success",
		},
	})

	return
}
