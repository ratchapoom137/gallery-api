package handlers

import (
	"api/context"
	"api/models"
	"api/services"
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type GalleryListWithInfo struct {
	GalleryListOutput []GalleryListOutput `json:"gallery_list"`
	ImgTotal          int                 `json:"img_total"`
}

type GalleryListOutput struct {
	ID          int            `json:"id"`
	UserID      int            `json:"user_id"`
	Name        string         `json:"name"`
	Images      []models.Image `json:"images"`
	ImagesTotal int            `json:"images_total"`
	IsPublic    bool           `json:"is_public"`
	CreatedBy   int            `json:"created_by"`
	UpdatedBy   int            `json:"updated_by"`
	CreatedAt   time.Time      `json:"created_at"`
	UpdatedAt   time.Time      `json:"updated_at"`
	DeletedAt   *time.Time     `json:"deleted_at,omitempty"`
}

// CreateGalleryHandler -
func CreateGalleryHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)
	var gallery models.Gallery
	if err := c.BindJSON(&gallery); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}
	gallery.UserID = userLogin.ID
	gallery.CreatedBy = userLogin.ID
	gallery.UpdatedBy = userLogin.ID

	if err := services.CreateGallery(&gallery); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}
	c.JSON(200, APIResult{
		Data: gallery.ID,
		Status: APIResultStatus{
			Code:    "GALLERY_CREATE_SUCCESS",
			Message: "Gallery create success",
		},
	})

	return
}

// GetGalleryByIDHandler -
func GetGalleryByIDHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	gallery, err := services.GetGalleryByID(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}

	if !gallery.IsPublic && gallery.UserID != userLogin.ID {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "No permission for get this gallery",
		})
		return
	}

	for index, _ := range gallery.Images {
		gallery.Images[index].URL = gallery.Images[index].FilePath()
	}

	c.JSON(201, APIResult{
		Data: gallery,
		Status: APIResultStatus{
			Code:    "GALLERY_SEARCH_SUCCESS",
			Message: "Gallery search success",
		},
	})

	return
}

// GetGalleryListHandler -
func GetGalleryListHandler(c *gin.Context) {

	userLogin := context.GetUserLogin(c)

	userID, userIDOK := c.GetQuery("user_id")
	name, nameOK := c.GetQuery("name")
	limit, limitOK := c.GetQuery("limit")
	offset, offsetOK := c.GetQuery("offset")
	isPublic, isPublicOK := c.GetQuery("is_public")

	filter := map[string]interface{}{}

	if nameOK {
		filter["name"] = name
	}
	if !limitOK && !offsetOK {
		limit = "-1"
		offset = "-1"
	}
	if isPublicOK {
		filter["is_public"] = isPublic
	}
	if !isPublicOK {
		filter["user_id"] = userLogin.ID
		if userIDOK {
			filter["user_id"], _ = strconv.Atoi(userID)
		}
	}
	filter["limit"] = limit
	filter["offset"] = offset

	galleries, err, total := services.GetGalleryList(filter)

	var galleriesOutput []GalleryListOutput
	jsonData, _ := json.Marshal(&galleries)
	json.Unmarshal(jsonData, &galleriesOutput)

	var galleryListWithInfo GalleryListWithInfo

	// Loop for count total image each Gallery
	for index, gl := range galleriesOutput {
		galleryListWithInfo.ImgTotal = galleryListWithInfo.ImgTotal + len(gl.Images)
		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(galleriesOutput[index].Images), func(i, j int) {
			galleriesOutput[index].Images[i], galleriesOutput[index].Images[j] = galleriesOutput[index].Images[j], galleriesOutput[index].Images[i]
		})
		if len(galleriesOutput[index].Images) > 0 {
			if len(galleriesOutput[index].Images) <= 3 {
				galleriesOutput[index].Images = galleriesOutput[index].Images[0:len(galleriesOutput[index].Images)]
			}
			if len(galleriesOutput[index].Images) >= 4 {
				galleriesOutput[index].Images = galleriesOutput[index].Images[0:4]
			}
		}
	}

	// Loop for file path each image
	for glIndex, gl := range galleriesOutput {
		for imgIndex, _ := range gl.Images {
			galleriesOutput[glIndex].Images[imgIndex].URL = galleriesOutput[glIndex].Images[imgIndex].FilePath()
		}
		galleriesOutput[glIndex].ImagesTotal = len(galleries[glIndex].Images)
	}

	galleryListWithInfo.GalleryListOutput = galleriesOutput
	if len(galleriesOutput) <= 0 {
		galleryListWithInfo.GalleryListOutput = []GalleryListOutput{}
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, APIResult{
		Data:  galleryListWithInfo,
		Total: total,
		Status: APIResultStatus{
			Code:    "GALLERY_SEARCH_SUCCESS",
			Message: "Gallery search success",
		},
	})

	return
}

// DeleteGalleryByIDHandler -
func DeleteGalleryByIDHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	galleryID, err, total := services.DeleteGalleryByID(id, userLogin)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, APIResult{
		Data:  galleryID,
		Total: total,
		Status: APIResultStatus{
			Code:    "GALLERY_DELETE_SUCCESS",
			Message: "Gallery delete success",
		},
	})

	return
}

func UpdateGalleryHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}
	var gallery models.Gallery
	if err = c.BindJSON(&gallery); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	gallery.ID = id

	err = services.UpdateGallery(gallery, userLogin)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}

	c.JSON(200, APIResult{
		Status: APIResultStatus{
			Code:    "GALLERY_UPDATE_SUCCESS",
			Message: "Gallery update success",
		},
	})

	return
}

func UpdateGalleryPublicHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}
	var gallery models.Gallery
	if err = c.BindJSON(&gallery); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	gallery.ID = id

	err = services.UpdateGalleryPublic(gallery, userLogin)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}

	c.JSON(200, APIResult{
		Status: APIResultStatus{
			Code:    "GALLERY_UPDATE_SUCCESS",
			Message: "Gallery update success",
		},
	})

	return
}
