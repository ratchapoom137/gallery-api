package handlers

import (
	"api/context"
	"api/models"
	"api/services"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func CreateLikeHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	imageID, err := strconv.Atoi(c.Param("image_id"))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	var like models.Like
	like.ImageID = imageID
	like.UserID = userLogin.ID

	if err := services.CreateLike(&like); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, APIResult{
		Status: APIResultStatus{
			Code:    "LIKE_CREATE_SUCCESS",
			Message: "Like create success",
		},
	})

	return
}

func DeleteLikeHandler(c *gin.Context) {
	userLogin := context.GetUserLogin(c)

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	var like models.Like
	like.ID = id
	like.UserID = userLogin.ID

	if err := services.DeleteLike(&like); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, APIResult{
		Status: APIResultStatus{
			Code:    "LIKE_DELETE_SUCCESS",
			Message: "Like delete success",
		},
	})

	return
}
