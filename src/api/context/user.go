package context

import (
	"api/models"

	"github.com/gin-gonic/gin"
)

const key string = "userLogin"

func SetUser(c *gin.Context, user *models.User) {
	c.Set(key, user)
}

func GetUserLogin(c *gin.Context) *models.User {
	user, ok := c.Value(key).(*models.User)
	if !ok {
		return nil
	}
	return user
}