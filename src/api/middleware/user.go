package middleware

import (
	"api/context"
	"api/header"
	"api/services"

	"github.com/gin-gonic/gin"
)

func RequireUser(c *gin.Context) {
	token := header.GetToken(c)
	if token == "" {
		c.Status(401)
		c.Abort()
		return
	}

	filter := map[string]interface{}{}
	filter["token"] = token
	user, _, _ := services.GetUserList(filter)
	if len(user) <= 0 {
		c.Status(401)
		c.Abort()
		return
	}

	context.SetUser(c, &user[0])
}
